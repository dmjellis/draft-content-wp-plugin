

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

	<?php
		
	//$page_data = thirty8_page_data(); 
	
	$dcp_draft_author = '';
	
	// Get the author responsible for this content
	$dcp_draft_author = get_field('draft_author');
	
	$dcp_draft_author_name = $dcp_draft_author['user_nicename'];
	
	?>
	
	<h2>Draft Status</h2>

	<p>Author responsible: <?php echo $dcp_draft_author_name;?></p>


<!--
  <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a>
  
-->  
  
</div>


<div class="dcp-sidebar">
	<span style="font-size:20px;cursor:pointer" onclick="openNav()">open &raquo;</span>
</div>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}
</script>