<div id="latest-posts-widget" class="widget">
	



<?php

$draftstatus = get_field("draft_status");

$bgcolor = "#f5f5f5";
$statustext = "";


$richtextcount = 0;
$attachmentcount = 0;


switch($draftstatus)
{

	case "":
	
		$bgcolor = "#FFFAFA";
		$bordercolor = "#CC0000";
		$statustext = "No content submitted yet";
	
	break;
	
	case "none":
	
		$bgcolor = "#FFFAFA";
		$bordercolor = "#CC0000";
		$statustext = "No content submitted yet";
	
	break;	
	
	case "awaitingsignoff":
	
		$bgcolor = "#FFFAFA";
		$bordercolor = "#FFB56C";
		$statustext = "Awaiting sign-off";
	

	break;	
	
	case "signedoff":
	
		$bgcolor = "#FFFAFA";
		$bordercolor = "#80CC80";
		$statustext = "Signed off";

	break;			

	case "draft":
	
		$bgcolor = "#FFFAFA";
		$bordercolor = "#FFB56C";
		$statustext = "Draft";
	

	break;	

	case "complete":
	
		$bgcolor = "#FFFAFA";
		$bordercolor = "#E6FAFF";
		$statustext = "Complete";
	

	break;	

}
	
?>

		<div style="font-size:80%; padding:10px; margin-bottom:5px; border:1px dashed <?php echo $bordercolor;?>; background-color:<?php echo $bgcolor;?>">
		
			<h3>Submitted content</h3>
			
			<?php 
			
				If ($statustext)
				{
					echo "<p><strong>Status: </strong>" . $statustext . "</p>";
				}	
				
				
				while(has_sub_field("draft_page_content")): 	
				
			
					if( get_row_layout() == 'richtext_content_block' )
					{
			
			        	$richtextcount = $richtextcount + 1;
			
					}
			
					if( get_row_layout() == 'attachment_content' )
					{

			        	$attachmentcount = $attachmentcount + 1;
			
					}		
			
				endwhile;
				
				If($richtextcount > 0)
				{
					echo "&raquo; Rich text: " . $richtextcount . "<br />";
				}
			
				
				If($attachmentcount > 0)
				{
					echo "&raquo; Attachments: " . $attachmentcount . "<br />";
				}
								
			
				echo "<hr />";
			
				edit_post_link('&raquo; Edit draft content', '<p>', '</p>');
			
			
			?>
		
		
		</div>
	
</div>
