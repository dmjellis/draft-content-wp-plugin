<?php	
		
	
function thirty8_include_acf()
{

	// Test to see if ACF is already installed - if so, don't include it
	if( class_exists('acf') ) 
    {	  
	    // for testing   
		//echo '<p style="margin-left:200px">ACF is already installed!</p>';
	}
	else
	{
		// ACF isn't already installed - so conditionally include it if we need it
		
		$include_acf = false;
		
		$this_url = $_SERVER['REQUEST_URI'];
		$this_url = str_replace(home_url(), '', $this_url);
			
		if($this_url == '/wp-admin/admin.php?page=draft_content')
		{
			$include_acf = 1;
		}
	
		if($this_url == '/wp-admin/admin.php?page=draft_content_report')
		{		
			$include_acf = 1;
		}
	
		if(strpos($this_url, 'edit.php?post_type=page'))
		{
			$include_acf = 1;
		}
	
		if(strpos($this_url, 'post.php'))
		{
			$include_acf = 1;
		}
	}
	
	
	return $include_acf;
	
}	
	

function thirty8_page_data()
{
	
	$total_page_count = 0;
	$pages_with_summary = 0;
	$pages_with_ok_summary_length = 0;
	$pages_with_featured_image = 0;

	$pages_with_status_nocontent = 0;
	$pages_with_status_draft = 0;
	
	$args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'hierarchical' => 1,
		'exclude' => '',
		'include' => '',
		'meta_key' => '',
		'meta_value' => '',
		'authors' => '',
		'child_of' => 0,
		'parent' => -1,
		'exclude_tree' => '',
		'number' => '',
		'offset' => 0,
		'post_type' => 'page',
		'post_status' => 'publish'
	); 
	$pages = get_pages($args);
	
	foreach($pages as $page)
	{		
		$total_page_count++;	
		$page_id = $page->ID;
		
		
		// Page Summary stuff
		$page_summary = get_field("page_summary",$page_id);

		if($page_summary)
		{
			$pages_with_summary++;
			if(strlen($page_summary)<150)
			{
				$pages_with_ok_summary_length++;
			}
		}
		
		// Page Featured Image
		
		if(has_post_thumbnail($page_id))
		{
			$pages_with_featured_image++;
		}
		
		// Pages with 'no content' status

		if(get_field("draft_status",$page_id) == 'none')
		{
			$pages_with_status_nocontent++;
		}		
		
		// Pages with 'draft' status
		
		if(get_field("draft_status",$page_id) == 'draft')
		{
			$pages_with_status_draft++;
		}
		
		$page_data['page_title'] = get_the_title($page_id);
		
		
	}

	$page_data['page_count'] = $total_page_count;
	$page_data['page_summary_count'] = $pages_with_summary;
	$page_data['page_summary_length_ok_count'] = $pages_with_ok_summary_length;
	$page_data['page_featured_image_count'] = $pages_with_featured_image;
	
	$page_data['pages_with_no_content'] = $pages_with_status_nocontent;
	$page_data['pages_with_status_draft'] = $pages_with_status_draft;
	
	return $page_data;
	
}



?>