<html>
  <head>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Mushrooms', 7],
          ['Onions', 1],
          ['Olives', 1],
          ['Zucchini', 1],
          ['Pepperoni', 2]
        ]);

        // Set chart options
        var options = {'title':'Content Status',
                       'width':400,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    
    
    <style>
	    
	ul li
	{
		list-style:disc;
		margin-left:30px;
	}    
	    
	</style>
    
    
  </head>

  <body>
    
	<h2>Content Report</h2>
	
	<hr/>
	
	<?php
	
	// Fetch the top level stuff from our page data function
	
	
	$page_data = thirty8_page_data(); 
	
	$total_page_count = $page_data['page_count'];
	$pgs_nocontent = $page_data['pages_with_no_content'];
	$pgs_draft = $page_data['pages_with_status_draft'];
	
	$pc_nocontent = round(($pgs_nocontent / $total_page_count),2);
	
	$pc_draft = round(($pgs_draft / $total_page_count),2);

	?>
	
	<h3>Summary</h3>
		
		<p>
			<ul>
				<li><strong>Total Pages:</strong> <?php echo $total_page_count;?></li>
				<li><strong>Pages flagged <em>no content</em>:</strong> <?php echo $pgs_nocontent;?> [<?php echo $pc_nocontent;?>%]</li>
				<li><strong>Pages flagged <em>draft</em>:</strong> <?php echo $pgs_draft;?> [<?php echo $pc_draft;?>%]</li>
			</ul>
		
		</p>
		
	

	<?php
	
	/*
	
	echo '<p><strong>Pages with summary field: </strong>' . $page_data['page_summary_count'] . '</p>';
	
	echo '<p><strong>Pages with ok length summary field: </strong>' . $page_data['page_summary_length_ok_count'] . '</p>';
	
	echo '<p><strong>Pages with featured image: </strong>' . $page_data['page_featured_image_count'] . '</p>';
	
	*/
	
	
	?>    

    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
    
    <hr />
    
    <h3>Detailed View</h3>

    <?php
	    
	    print_r($page_data);
	    
	?>
    
    
  </body>
</html>




