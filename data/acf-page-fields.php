<?php

$somevariable = $_SERVER['REQUEST_URI'];

$draft_doc_url = 'https://docs.google.com/document/d/1pL39JnBtnYjeegkoEVbVVEq1gl0QbUf3F4SR_EgX2p4/edit';


if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_page-field-group',
		'title' => 'Draft Content',
		'fields' => array (
		array (
			'key' => 'field_55dc6a51af2a3',
			'label' => 'Draft author',
			'name' => 'draft_author',
			'type' => 'user',
			'instructions' => 'Choose who is responsible for providing this draft content',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'role' => '',
			'allow_null' => 1,
			'multiple' => 0,
		),
		array (
			'key' => 'field_55dc6a1daf2a2',
			'label' => 'Draft Intro Text',
			'name' => 'draft_intro_text',
			'type' => 'textarea',
			'instructions' => 'A short description of this page - try and keep this to 150chrs',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),		
		array (
			'key' => 'field_55db2ba3408f1',
			'label' => 'Draft status',
			'name' => 'draft_status',
			'type' => 'select',
			'instructions' => 'Choose what the status is of the content that has been provided',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'none' => 'No content provided yet',
				'awaitingsignoff' => 'Content is awaiting editing / sign-off',
				'draft' => 'Content is in draft form',
				'signedoff' => 'Content is signed off',
				'complete' => 'Content has been copied to site',
			),
			'default_value' => array (
				'none' => 'none',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_5a11eefd2247a',
			'label' => 'Draft Content File',
			'name' => 'draft_content_file',
			'type' => 'file',
			'instructions' => 'Upload or select your draft content to attach to this page',
			'save_format' => 'object',
			'library' => 'all',
		),								
		array(
			'key' => 'field_5aad5c9151ccc',
			'label' => 'Document Draft',
			'name' => '',
			'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'message' => '
			
			<h1>Some message in here</h1>

			<p><a href="javascript:thirty8openmodal(\'url\')">click me</a></p>
			
			<p>' . $somevariable . '</p>
			

			<iframe src="' . $draft_doc_url . '" width="100%" height="600"></iframe>-->',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
		),
		
		/* I just dunno about this - having a relational "draft content" post type...??
			array (
				'key' => 'field_5a11ecebe5c42',
				'label' => 'Draft Content',
				'name' => 'draft_content',
				'type' => 'post_object',
				'instructions' => 'Attach your draft content to this page',
				'post_type' => array (
					0 => 'thirty8_draftcontent',
				),
				'taxonomy' => array (
					0 => 'all',
				),
				'allow_null' => 1,
				'multiple' => 1,
			),		
		*/

		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 1,
				),
			),			
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}



?>