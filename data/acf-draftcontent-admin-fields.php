<?php

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_draft-content-options',
		'title' => 'Draft Content Options',
		'fields' => array (
			array (
				'key' => 'field_5b00325139b11',
				'label' => 'Some field',
				'name' => 'some_field',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

?>