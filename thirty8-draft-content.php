<?php

/**
 * Plugin Name: Thirty8 Draft Content Plugin 
 * Plugin URI: http://thirty8.co.uk
 * Description: Draft content tool for WordPress
 * Version: 0.3
 * Author: Mike Ellis / Thirty8 Digital
 */

// Include functions
include_once('includes/functions.php');

class Thirty8DraftContent
{

	public function __construct() 
	{
		add_action( 'admin_menu', array( $this, 'create_plugin_settings_pages' ) );
		add_action('get_header',array( $this, 'thirty8_load_dc_sidebar' ) );		


add_action( 'admin_bar_menu', 'add_menu_item',900);

function add_menu_item($bar)
{
	$menu_id = 'dcp';
	/*
    $bar->add_menu( array(
        'id'     => 'wpse',
        'parent' => null,
        'group'  => null,
        'title'  => __( 'Draft Content', 'some-textdomain' ),
        'href'   => '',
        'meta'   => array(
            'target'   => '_self',
            'title'    => __( 'Hello', 'some-textdomain' ),
            'html'     => '<a href="javascript:openNav()">click me</a>',
            'class'    => 'wpse--item',
            'rel'      => 'friend',
            'onclick'  => 'openNav()',
            'tabindex' => PHP_INT_MAX,
        ),
    ) );
    */

    
//$menu_id = 'dwb';
	$bar->add_menu(array('id' => $menu_id, 'title' => __('Draft Content'), 'href' => '/'));
	$bar->add_menu(array(
		'parent' => $menu_id, 
		'title' => __('Show Detail'), 
		'id' => 'dcp-sidebar', 
		'href' => '', 
		'meta' => array(
			'onclick' => 'openNav();return false',
			)
		));
	$bar->add_menu(array('parent' => $menu_id, 'title' => __('Drafts'), 'id' => 'dwb-drafts', 'href' => 'edit.php?post_status=draft&post_type=post'));
	$bar->add_menu(array('parent' => $menu_id, 'title' => __('Pending Comments'), 'id' => 'dwb-pending', 'href' => 'edit-comments.php?comment_status=moderated'));
	
	    
};
		
		
		
			// need a conditional to check if ACF is already installed
			// also to deal with ACF Pro vs ACF, cos they break in different ways

/*
	
			if ( is_plugin_active( 'thirty8-draftcontent/thirty8-draftcontent.php' ) ) 
			{
				$pluginsidebarpath = ABSPATH . "wp-content/plugins/thirty8-draftcontent/draftcontent-sidebar.php";
				include($pluginsidebarpath);	
			} 
			     	
	
	
	
*/
		
			$include_acf = true;
		
			if($include_acf)
			{
		
			//echo '<p style="margin-left:200px">ACF has been included!</p>';
			
			include_once( plugin_dir_path( __FILE__ ) . 'vendor/advanced-custom-fields/acf.php' );
			add_filter( 'acf/settings/path', array( $this, 'update_acf_settings_path' ) );
			add_filter( 'acf/settings/dir', array( $this, 'update_acf_settings_dir' ) );
			$this->setup_options();
			add_action( 'admin_init', array( $this, 'add_acf_variables' ) );
			
			// This should hide the ACF field stuff from menu...but doesn't... >>
			//add_filter( 'acf/settings/show_admin', '__return_false' );	
			
			// Include ACF fields for the site's pages
			include_once('data/acf-page-fields.php');			
			
			}

					

	}	

	public function add_acf_variables() 
	{
		acf_form_head();
	}

	public function setup_options() 
	{
		include_once('data/acf-draftcontent-admin-fields.php');		
	}
	
	public function update_acf_settings_path( $path ) {
		$path = plugin_dir_path( __FILE__ ) . 'vendor/advanced-custom-fields/';
		return $path;
	}
	
	public function update_acf_settings_dir( $dir ) {
		$dir = plugin_dir_url( __FILE__ ) . 'vendor/advanced-custom-fields/';
		return $dir;
	}	
	
	public function create_plugin_settings_pages() 
	{
		// Add the menu item and page
		$page_title = 'Draft Content';
		$menu_title = 'Draft Content';
		$capability = 'manage_options';
		$slug = 'draft_content';
		$callback = array( $this, 'thirty8_draftcontent_homepage' );
		$icon = 'dashicons-admin-plugins';
		$position = 100;
	
		add_menu_page( $page_title, $menu_title, $capability, $slug, $callback, $icon, $position );
		add_submenu_page('draft_content', 'Report', 'Report', 'manage_options', 'draft_content_report','thirty8_draftcontent_report');

		function thirty8_draftcontent_report()
		{
			include('admin/report.php');
		}

		
	}
	
	
	
	
	public function thirty8_draftcontent_homepage()
	{
		include('admin/index.php');
		do_action('acf/input/admin_head'); // Add ACF admin head hooks
		do_action('acf/input/admin_enqueue_scripts'); // Add ACF scripts
	
	
		// Include some plugin options
		// Fields are determined by data/acf-draftcontent-admin-fields.php
	
		$options = array(
			'id' => 'acf-form',
			'post_id' => 'options',
			'new_post' => false,
			'field_groups' => array( 'acf_draft-content-options' ),
			'return' => admin_url('admin.php?page=draft_content'),
			'submit_value' => 'Update',
		);
		acf_form( $options );
		

	}	


	public function thirty8_load_dc_sidebar()
	{
		include('theme-includes/themecss.css');
		include('theme-includes/draftcontent-sidebar.php');
	}

	
}


new Thirty8DraftContent();

/*
	
	// Might not want this right now...

function create_dc_post_type() 
{
  register_post_type( 'thirty8_draftcontent',
    array(
      'labels' => array(
        'name' => __( 'Draft Content' ),
        'singular_name' => __( 'Draft Content' )
      ),
      'public' => true,
      'has_archive' => true,
    )
  );
}
add_action( 'init', 'create_dc_post_type' );		


<script>
function thirty8openmodal() 
{
    var myWindow = window.open("' ", "", "width=800,height=600");
}
</script>
*/
?>